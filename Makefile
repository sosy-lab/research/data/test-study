required: setup statistics produced-data/macro.tex

all: prepare-original-data testcov-on-branch-test-suites required

# ----------------------------------------------------------------------------------------
# 4. Create the final data for the paper:
# Only this step is necessary as the processed results are already available in directory raw-data/

## 4.1 Create sv-benchmarks statistics
produced-data/macro.tex: scripts/Statistics.py
	python3 scripts/Statistics.py > $@

## 4.2 Create data on found bugs
.PHONY: statistics
statistics: scripts/Postprocess.py table-defs/all.table.csv table-defs/ReachSafety-Arrays.table.csv table-defs/ReachSafety-Arrays-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-BitVectors.table.csv table-defs/ReachSafety-BitVectors-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-ControlFlow.table.csv table-defs/ReachSafety-ControlFlow-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-ECA.table.csv table-defs/ReachSafety-ECA-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-Floats.table.csv table-defs/ReachSafety-Floats-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-Hardware.table.csv table-defs/ReachSafety-Hardware-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-Heap.table.csv table-defs/ReachSafety-Heap-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-Loops.table.csv table-defs/ReachSafety-Loops-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-ProductLines.table.csv table-defs/ReachSafety-ProductLines-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-Recursive.table.csv table-defs/ReachSafety-Recursive-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-Sequentialized.table.csv table-defs/ReachSafety-Sequentialized-with-wtt-validation-explicit.table.csv table-defs/ReachSafety-XCSP.table.csv table-defs/ReachSafety-XCSP-with-wtt-validation-explicit.table.csv table-defs/SoftwareSystems-BusyBox.table.csv table-defs/SoftwareSystems-BusyBox-with-wtt-validation-explicit.table.csv table-defs/SoftwareSystems-DeviceDriversLinux64.table.csv table-defs/SoftwareSystems-DeviceDriversLinux64-with-wtt-validation-explicit.table.csv
	python3 scripts/Postprocess.py

table-defs/all.table.csv: table-defs/all.xml
	tools/benchexec/bin/table-generator -x $<

table-defs/%.table.csv: table-defs/%.xml
	tools/benchexec/bin/table-generator --common -x $<

# ----------------------------------------------------------------------------------------
# 3. Run TestCov on the test suites generated for branch coverage,
# but with coverage criterion coverage-error-call
testcov-on-branch-test-suites: tools/testcov comp-results/testcomp23-test-suites
#  if you have access to our verifier cloud, you can use
	scripts/run_testcov_vcloud.sh
# scripts/configure_cgroups.sh
# scripts/run_testcov_local.sh
# Make sure that the new testcov run results are also adjusted to match the SV-COMP results
	python3 scripts/PrepareComps.py

# ----------------------------------------------------------------------------------------
# 2. Adjust Test-Comp results and SV-COMP results to match.
# More information can be found in the comments of the called script.
# The script puts postprocessed data into directory 'raw-data/'.
.PHONY: prepare-original-data
prepare-original-data: comp-results/svcomp23-results comp-results/testcomp23-results comp-results/testcomp23-test-suites
	python3 scripts/PrepareComps.py

# ----------------------------------------------------------------------------------------
# 1. Get the official competitions' data
# This step is optional as the processed results are already available in directory raw-data/

## 1.1 Download testcomp23-witnesses.zip from https://zenodo.org/record/7701126
# This job is implied if the TestCov-results are to be repeated
comp-results/testcomp23-witnesses.zip:
	wget https://zenodo.org/record/7701126/files/testcomp23-witnesses.zip?download=1 -O $@

## 1.2 Download testcomp23-results.zip from https://zenodo.org/record/7701122
# This job is implied if the TestCov-results are to be repeated
comp-results/testcomp23-results.zip:
	wget https://zenodo.org/record/7701122/files/testcomp23-results.zip?download=1 -O $@

## 1.3 Download svcomp23-results.zip from https://zenodo.org/record/7627787
comp-results/svcomp23-results.zip:
	wget https://zenodo.org/record/7627787/files/svcomp23-results.zip?download=1 -O $@

## 1.4.1 Unpack test-comp results
comp-results/testcomp23-results: comp-results/testcomp23-results.zip
	mkdir -p $@
	unzip -n $< -d $@

comp-results/testcomp23-witnesses: comp-results/testcomp23-witnesses.zip
	mkdir -p $@
	unzip -n $< -d $@

## 1.4.2 Unpack test suites of Test-Comp into human-readable files
comp-results/testcomp23-test-suites: comp-results/testcomp23-results comp-results/testcomp23-witnesses comp-results/unpackFileStores.py
	cd comp-results \
		&& ./unpackFileStores.py testcomp23-results/results-verified/*.json.gz \
			--output-dir testcomp23-test-suites

## 1.5 Unpack SV-COMP data
comp-results/svcomp23-results: comp-results/svcomp23-results.zip
	mkdir -p $@
	unzip -n $< -d $@


# ----------------------------------------------------------------------------------------
# 0. Make sure this repository is fully set up
.PHONY: setup
setup:
	git submodule update --init

tools/testcov: test-comp/archives/2023/val_testcov.zip
	cd tools && unzip ../$<

