#!/usr/bin/python3

import argparse
from functools import lru_cache
import sys
import numpy
import math
from typing import NamedTuple, Dict, Sequence, Set, Tuple, Union
from pathlib import Path
import yaml


def LOC(fname) -> int:
    assert not fname.name.endswith(".yml")
    with open(fname) as f:
        for i, _ in enumerate(f, 1):
            pass
    return i


@lru_cache
def parse_yaml(filepath: Path) -> Dict:
    with open(filepath) as f:
        return yaml.safe_load(f)


def round_sigfigs(num, sig_figs):
    if num != 0:
        return round(num, -int(math.floor(math.log10(abs(num))) - (sig_figs - 1)))
    else:
        return 0  # Can't take the log of 0


def parse(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--tex",
        dest="print_tex",
        default=False,
        action="store_true",
        help="Print the statistics as LaTeX table",
    )
    parser.add_argument(
        "set_files", nargs="+", help="sv-benchmarks .set files to analyze"
    )
    return parser.parse_args(argv)


def get_set_name(filepath: Path) -> str:
    basename = filepath.name
    return ".".join(basename.split(".")[:-1])


def get_input_file(task: Path) -> Path:
    content = parse_yaml(task)
    return task.resolve().parent / content["input_files"]


def get_tasks(set_file: Path, prop: str = "coverage-error-call.prp") -> Sequence[Path]:
    def contains_property(task, prop):
        content = parse_yaml(task)
        return any(prop in p["property_file"] for p in content["properties"])

    with open(set_file) as inp:
        patterns = [
            l.strip()
            for l in inp.readlines()
            if l.strip() and not l.strip().startswith("#")
        ]
    set_dir = set_file.resolve().parent
    tasks = set()
    for task_pattern in patterns:
        tasks |= set(set_dir.glob(task_pattern))
    return {get_input_file(t) for t in tasks if contains_property(t, prop)}


LinesOfCode = int


class Statistics(NamedTuple):
    tasks: Set[Tuple[str, LinesOfCode]]
    LOC_sum: int
    LOC_min: int
    LOC_max: int
    LOC_avg: int
    LOC_median: float
    task_count: int


def collect_statistics(task_definitions: Sequence[Path]) -> Statistics:
    tasks_with_LOC = set()
    for task in task_definitions:
        tasks_with_LOC.add((task, LOC(task)))
    all_locs = [l for _, l in tasks_with_LOC]

    return Statistics(
        tasks=tasks_with_LOC,
        LOC_sum=sum(all_locs),
        LOC_min=min(all_locs),
        LOC_max=max(all_locs),
        LOC_avg=numpy.mean(all_locs),
        LOC_median=numpy.median(all_locs),
        task_count=len(all_locs),
    )


def collect_statistics_from_set(set_files: Union[Sequence, Path]) -> Statistics:
    if isinstance(set_files, Sequence):
        data: Dict[str, Statistics] = {}
        for set_file in set_files:
            set_file = Path(set_file)
            set_name = get_set_name(set_file)
            if stats := collect_statistics_from_set(set_file):
                data[set_name] = stats

        data["Total"] = Statistics(
            tasks={(t, c) for d in data.values() for t, c in d.tasks},
            LOC_sum=sum([d.LOC_sum for d in data.values()]),
            LOC_min=min([d.LOC_min for d in data.values()]),
            LOC_max=max([d.LOC_max for d in data.values()]),
            LOC_avg=numpy.mean([l for d in data.values() for _, l in d.tasks]),
            LOC_median=numpy.median([l for d in data.values() for _, l in d.tasks]),
            task_count=sum([d.task_count for d in data.values()]),
        )
        return data

    set_file = set_files
    tasks = get_tasks(set_file)
    if not tasks:
        print(
            f"No relevant tasks found in set file {set_file}, skipping", file=sys.stderr
        )
        return None
    return collect_statistics(tasks)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = parse(argv)
    data = collect_statistics_from_set(args.set_files)

    if args.print_tex:
        for s, d in sorted(data.items(), key=lambda x: x[0]):
            for k, v in d.items():
                print("\\newcommand{\\" + s + k + "}{" + str(v) + "}")
    else:
        html_table = []
        html_table += ["<table class='statistics'>"]
        html_table += [
            "  <thead>",
            "    <tr>",
            "      <th rowspan='2' style='text-align: left'>Category</th>",
            "      <th rowspan='2'>Tasks</th>",
            "      <th colspan='5'>LOC</th>",
            "      <th rowspan='2'>C features</th>",
            "    </tr>",
            "    <tr>",
            "      <th>Sum</th>",
            "      <th>Min</th>",
            "      <th>Max</th>",
            "      <th>Avg</th>",
            "      <th>Median</th>",
            "    </tr>",
            "  </thead>",
            "  <tbody>",
        ]
        for s, d in sorted(data.items(), key=lambda x: x[0]):
            group = "group"
            html_table += ["    <tr>", "      <td class='{}'>{}</td>".format(group, s)]
            loc_keys = [
                (d.task_count, "count"),
                (d.LOC_sum, "count"),
                (d.LOC_min, "count"),
                (d.LOC_max, "count"),
                (d.LOC_avg, "measurement"),
                (d.LOC_median, "measurement"),
            ]
            for v, g in loc_keys:
                if "measurement_sig" == g:
                    v = str(round_sigfigs(v, 2))
                else:
                    v = str(int(round(v)))

                html_table += ["      <td class='{}'>{}</td>".format(g, v)]
            html_table += ["      <td></td>", "    </tr>"]
        html_table += ["  </tbody>", "</table>"]

        for line in html_table:
            print(line)

    return 0


if __name__ == "__main__":
    sys.exit(main())
