#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import bz2
from functools import partial
import itertools
import multiprocessing
from pathlib import Path
from typing import List, Set
from xml.etree import ElementTree as etree


def define_path(path):
    assert path.exists(), f"{path.resolve()} does not exist"
    return path


RESULTS_DIR = define_path(Path(__file__).parent.parent / "comp-results")
OUTPUT_DIR = RESULTS_DIR.parent / "raw-data"
SVCOMP_RESULTS = define_path(RESULTS_DIR / "svcomp23-results")
TESTCOMP_RESULTS = define_path(RESULTS_DIR / "testcomp23-results")


# In[ ]:


def open_file(path, mods="r"):
    assert "w" in mods or path.exists()
    if path.name.endswith(".bz2"):
        return bz2.open(path, mods)
    else:
        return open(path, mods)


def parse_xml(path) -> etree.Element:
    with open_file(path) as inp:
        return etree.XML(inp.read())


def write_xml(xml: etree.Element, path) -> None:
    with open_file(path, mods="wb") as outp:
        etree.ElementTree(xml).write(outp)


# In[ ]:


# We want to reuse the original SV-COMP23 and Test-Comp23 results data.
# Our goal is to compare SV-COMP participants and Test-Comp participants
# with respect to their bug-finding capabilities.
# This means that we are interested in program property 'unreach-call':
# RQ1. Can a verifier/tester successfully tell us that there is a bug in the program?
# RQ2. Can a successful formal verifier provide a witness to this?
#
# SV-COMP data already exists for program property 'unreach-call'.
# Test-Comp used coverage criterion 'coverage-error-call', but a success there implies a success in 'unreach-call'.
#
#   Careful: it's not the other way around, i.e., unreach-call does not imply coverage-error-call!
#   Because coverage-error-call requires the creation of a test suite that covers the error call.
#
# So, for RQ1, we have to take all results data of Test-Comp (todo-1),
# and replace property 'coverage-error-call'
# with property 'unreach-call' (todo-2).
# In addition, we have to please BenchExec's table generator that we want to use later:
# We have to change the test-generation verdicts from 'done' to 'false(unreach-call)' (todo-3)
# to avoid an assertion failure in table-generator code,
# and we have to add attribute 'expectedVerdict="false"' to each run element (todo-4)
# to make table-generator match runs from SV-COMP and Test-Comp.


# (todo-1): There's two ways to consider the data:
# 1. We can consider the original test-generation data (files in 'results-verified' **without** suffix '.fixed.xml.bz2')
#    and the original test-validation data (files in directory 'results-validated'). We then have to merge the two.
# 2. We can consider the already-fixed test-generation data (files in 'results-verified) **with** suffix '.fixed.xml.bz2').
#
# All files miss information about the test-suite size, and we are mostly not interested in the measurements of test-suite validation.
# So there's (at the moment) no need to go with the more complicated option 1. We choose option 2.
# There's data for property coverage-branches and coverage-error-call.
# Here, we are only interested in coverage-error-call.
def get_testcomp_data(data_directory):
    # return everything as a list instead of the generator created by .glob,
    # so that we don't have to worry about how often the sequence of files is accessed.
    return list(
        data_directory.glob("**/*Test-Comp23_coverage-error-call*.bz2.fixed.xml.bz2")
    )


# (todo-2):
# The *.fixed.xml.bz2 XMLs look like this:

# ```
# <?xml version="1.0" ?>
# <result benchmarkname="FuSeBMC" date="2021-12-16 03:43:34 CET" starttime="2021-12-16T03:44:19.190571+01:00" tool="FuSeBMC" version="v.4.1.14" toolmodule="benchexec.tools.fusebmc" generator="BenchExec 3.11-dev" displayName="FuSeBMC" memlimit="15000000000B" timelimit="900s" cpuCores="8" options="-s incr" block="ReachSafety-Arrays" name="Test-Comp23_coverage-error-call.ReachSafety-Arrays" endtime="2021-12-16T09:43:49.953400+01:00">
#   <description>Provenance information:
# Benchmark executed
# for Test-Comp 2023, https://test-comp.sosy-lab.org/2023/
# by dbeyer@wbeyer1.sosy.ifi.lmu.de
# on 2021-12-16T03:43+01:00
# based on the components
# TODO UPDATE
# git@gitlab.com:sosy-lab/test-comp/archives-2023  git-describe: testcomp23-rc.1-0-gbcfe424
# git@gitlab.com:sosy-lab/benchmarking/sv-benchmarks  git-describe: testcomp23-rc.1-0-g56689c335c
# https://gitlab.com/sosy-lab/software/benchexec  git-describe: 3.10-8-g0e0f79cf
# git@gitlab.com:sosy-lab/benchmarking/competition-scripts  git-describe: testcomp23-rc.1-1-ge588a36
# git@gitlab.com:sosy-lab/test-comp/bench-defs.git  git-describe: testcomp23-rc.1-0-gb5dda87
# Archive: FuSeBMC.zip  DATE: 2021-11-23T16:45+01:00  SHA1: c218a6468a...</description>
#   <columns>
#     <column title="status"/>
#     <column title="cputime"/>
#     <column title="walltime"/>
#   </columns>
#   <run name="../sv-benchmarks/c/array-examples/data_structures_set_multi_proc_ground-1.yml"
#       files="[../sv-benchmarks/c/array-examples/data_structures_set_multi_proc_ground-1.i]"
#       properties="coverage-error-call"
#       propertyFile="../sv-benchmarks/c/properties/coverage-error-call.prp">
#     <column title="cpuenergy" value="10576.803223J"/>
#     <column title="cputime" value="871.782540311s"/>
#     <column title="host" value="apollon040"/>
#     <column title="memory" value="1159086080B"/>
#     <column title="status" value="done"/>
#     <column title="walltime" value="880.8550136799458s"/>
#     <column title="category" value="correct" hidden="true"/>
#     <column title="cpuCores" value="0,4,1,5,2,6,3,7" hidden="true"/>
#     <column title="cpuenergy-pkg0-core" value="9703.324158J" hidden="true"/>
#     <column title="cputime-cpu0" value="1.208338842s" hidden="true"/>
#     <column title="score" value="1"/>
#     [...]
#   </run>
#   <run>
#   [...]
# </result>
#
# To replace 'coverage-error-call' with 'unreach-call',
# we have to adjust the values of attributes run.properties and run.propertyFile for each run element.
# We replace all information in-situ because it's easier.
def replace_cover_error(new_prop: str, xml: etree.Element) -> None:
    for run in xml.findall("run"):
        run.set("properties", new_prop)
        # keep the file path and the file suffix, only replace property
        run.set(
            "propertyFile",
            run.get("propertyFile").replace("coverage-error-call", new_prop),
        )


# (todo-3): For each run element with child <column title='category' value='correct'>,
# replace child <column title='status' value='*'> with value='false(unreach-call)'
def replace_verdict_for_successes(new_value: str, xml: etree.Element) -> None:
    for run in xml.findall("run"):
        if any(
            child.get("title") == "category" and child.get("value") == "correct"
            for child in run.findall("column")
        ):
            [
                child.set("value", new_value)
                for child in run.findall("column")
                if child.get("title") == "status"
            ]


# (todo-4): For each run element, add attribute expectedVerdict="false"
def add_expected_verdicts(xml: etree.Element) -> None:
    for run in xml.findall("run"):
        run.set("expectedVerdict", "false")


# In[ ]:


def main(data_directory=TESTCOMP_RESULTS, output_directory=OUTPUT_DIR):
    # (todo-1)
    for datafile in get_testcomp_data(data_directory):
        xml = parse_xml(datafile)
        # (todo-2)
        replace_cover_error("unreach-call", xml)
        # (todo-3)
        replace_verdict_for_successes("false(unreach-call)", xml)
        # (todo-4)
        add_expected_verdicts(xml)
        write_xml(xml, output_directory / datafile.name)


main()


# In[ ]:


# RQ 3: How good is the test-suite created for branch coverage in finding errors?
# For this, we do not have *.fixed.xml.bz2 files from Test-Comp,
# but had to run new TestCov runs with propery 'coverage-error-call' but the test suites
# created for coverage-branches.
# These are available in directory 'raw-data' as 'testcov-validate-test-suites.*.results.TESTGENERATOR-cov-error-with-branches.xml.bz2',
# where TESTGENERATOR is the all-uppercase name of the respective test generator. (todo-1)
#
# Most importantly, we want to get the total numbers for this.
# To get these, we don't have to put the test-suite generation and validation in relation,
# but it is good enough to only look at the validation.
# To make it comparable with SV-COMP runs, we have to do the same adjustments as for the previous Test-Comp runs. (todo-2)

# (todo-1): Get all files that fulfill glob pattern 'testcov-validate-test-suites.*.results.*-cov-error-with-branches.xml.bz2'
def get_testcov_data_for_branch_coverage(data_directory: Path) -> List[Path]:
    # return everything as a list instead of the generator created by .glob,
    # so that we don't have to worry about how often the sequence of files is accessed.
    return list(
        data_directory.glob(
            "testcov-validate-test-suites.*.results.*-cov-error-with-branches.*xml.bz2"
        )
    )


# to each run element with child <column title='status'>true</column>,
# add new element <column title="category" value="correct" hidden="true"/>
def set_category_for_status_true(xml: etree.Element) -> None:
    for run in xml.findall("run"):
        if any(
            child.get("title") == "status" and child.get("value") == "true"
            for child in run.findall("column")
        ):
            [
                c.set("value", "correct")
                for c in run.findall("column")
                if c.get("title") == "category"
            ]


# (todo-2.1): Do the same as in the beginning for Test-Comp result data.
# In addition, we have to set 'category' and adjust the status from 'true' to 'false(unreach-call)'.
# This is because TestCov reports status 'true' if a test suite covers error,
# but verification-tasks say 'false' if the error is reachable. (todo-2.2)
def adjust_and_write(datafile, output_directory):
    xml = parse_xml(datafile)
    # (todo-2.2)
    set_category_for_status_true(xml)
    # (todo-2.1)
    replace_cover_error("unreach-call", xml)
    replace_verdict_for_successes("false(unreach-call)", xml)
    add_expected_verdicts(xml)
    write_xml(xml, output_directory / datafile.name)


def main(data_directory=OUTPUT_DIR, output_directory=OUTPUT_DIR):
    adjust = partial(adjust_and_write, output_directory=output_directory)
    with multiprocessing.Pool(multiprocessing.cpu_count()) as p:
        p.map(adjust, get_testcov_data_for_branch_coverage(data_directory))


main()


# In[ ]:


# Test-Comp data is prepared for comparison with SV-COMP now.
# Before we compute a table that compares participants of Test-Comp and SV-COMP,
# we can do some optimization:
# 1. There are more tasks with property 'unreach-call' than with property 'coverage-error-call',
#    so there are tasks in the SV-COMP results data that will end up with empty rows for Test-Comp participants.
# 2. There are bugs in the sv-benchmarks repository: Tasks that have property 'unreach-call'
#    with expected verdict 'true' have property 'coverage-error-call', even though this can never be fulfilled.
#
# To avoid that we have to handle this later (or do some complicated calculations),
# we remove them from the SV-COMP/Test-Comp xml files before we build the table.

# (todo-1): Get all tasks used in Test-Comp's coverage-error-call
#     (todo-1.1): Read all Test-Comp XML files for coverage-error-call
#     (todo-1.2): Collect all task names from the run elements' attribute 'name'.
def get_individual(datafile, filter_func) -> Set[str]:
    xml = parse_xml(datafile)
    return {run.get("name") for run in xml.findall("run") if filter_func(run)}


def always_true(*args):
    del args
    return True


def get_testcomp_tasks(data_directory) -> Set[str]:
    getter = partial(get_individual, filter_func=always_true)
    all_sets = apply(getter, get_testcomp_data(data_directory))
    return {t for ts in all_sets for t in ts}


# (todo-2): Filter all relevant SV-COMP result files so that they only contain run elements with names that appear in Test-Comp.
#     (todo-2.1): Read all SV-COMP XML files for unreach-call
#     (todo-2.2): Filter the run elements in these XMLs

# ( todo-2.1)
def get_svcomp_data(data_directory) -> List[Path]:
    verifier_results = set(data_directory.glob("**/*SV-COMP23_unreach-call.*.bz2.fixed.xml.bz2"))
    # Since SV-COMP 23, the witness2-test validation runs are also available in a fixed.xml.bz2 version.
    # We do NOT want this version, because it only contains run results for valid runs.
    # This would destroy our later data aggregation with 'table-generator --common'.
    # So we make sure that we (also) get the unfixed result files.
    wtt_validation_results = set(data_directory.glob("**/*witness2test-validate*SV-COMP23_unreach-call.*.xml.bz2"))
    # return everything as a list
    # so that we don't have to worry about how often the sequence of files is accessed.
    return list(verifier_results | wtt_validation_results)


# (todo-2.2):
def filter_tasks(data: etree.Element, tasks: Set[str]) -> None:
    for run in data.findall("run"):
        if run.get("name") not in tasks:
            data.remove(run)


def apply(func, sequence) -> List[any]:
    # multiprocessing splits computation time in less than half (on our machine)
    with multiprocessing.Pool(multiprocessing.cpu_count()) as pool:
        # use list to trigger all map invocations, independent of underyling map implementation
        return list(pool.map(func, sequence))


def is_expected_verdict_false(task):
    return task.get("expectedVerdict") == "false"


# (todo-3): Filter all relevant SV-COMP and Test-Comp result files so that they only contain
# run elements with expectedVerdict='false' in the SV-COMP sets
def get_tasks_with_expected_verdict_false(data_directory) -> Set[str]:
    getter = partial(get_individual, filter_func=is_expected_verdict_false)
    all_sets = apply(getter, get_svcomp_data(data_directory))
    return {t for ts in all_sets for t in ts}


# In[ ]:


import re

benchmark_task_prefix = re.compile(r"^(\.\./)*sv-benchmarks/")


def create_filtered_version(svcomp_data, tasks_to_keep, output_directory) -> None:
    xml = parse_xml(svcomp_data)
    filter_tasks(xml, tasks_to_keep)
    if not xml.findall("run"):
        print(f"Filtered all runs, skipping {svcomp_data.name}")
        svcomp_data.unlink()
        return
    write_xml(xml, output_directory / svcomp_data.name)


# First, we should unify relative paths in run elements.
# This has two reasons:
# 1. We later do an equality check on task names for filtering,
#    so our runs must be named uniformly.
#    tasks_to_keep (this depends on the results files used to create tasks_to_keep)
# 2. table-generator can only identify that two runs represent the same verification task
#    if the name-attribute of the run elements is identical,
#    AND if the propertyFile of the run elements is identical.
# To avoid mishaps due to different relative paths, we adjust all name attributes
# and all propertyFile attributes
# to have prefix "../sv-benchmarks" (not more or less '..')
def unify_tasks(data: Path, output_directory: Path) -> None:
    xml = parse_xml(data)
    for run in xml.findall("run"):
        new_name = benchmark_task_prefix.sub("../sv-benchmarks/", run.get("name"))
        run.set("name", new_name)
        new_propertyfile = benchmark_task_prefix.sub(
            "../sv-benchmarks/", run.get("propertyFile")
        )
        run.set("propertyFile", new_propertyfile)
    write_xml(xml, output_directory / data.name)


def main(
    # reuse the files we already adjusted for correct property and expectedVerdict.
    # these are in OUTPUT_DIR.
    testcomp_directory=OUTPUT_DIR,
    svcomp_directory=SVCOMP_RESULTS,
    output_directory=OUTPUT_DIR,
):
    # we have to filter both SV-COMP and Test-Comp files because
    # Test-Comp results may contain tasks with expected verdict 'true'
    # (due to bugs in sv-benchmarks)
    all_data = itertools.chain(
        get_svcomp_data(svcomp_directory),
        get_testcomp_data(testcomp_directory),
        get_testcov_data_for_branch_coverage(testcomp_directory),
    )

    unify = partial(unify_tasks, output_directory=output_directory)
    apply(unify, all_data)
    # unify writes unified XML files to output_directory,
    # so we should continue with the (unified) XML files there.
    all_data = itertools.chain(
        get_svcomp_data(output_directory),
        get_testcomp_data(output_directory),
        get_testcov_data_for_branch_coverage(output_directory),
    )

    # (todo-1)
    testcomp_tasks = get_testcomp_tasks(output_directory)
    # (todo-3)
    false_tasks = get_tasks_with_expected_verdict_false(output_directory)

    # handle todo-1 and todo-3 in one step, to keep I/O operations low
    # we only want tasks that are from Test-Comp and have property unreach-call: 'false',
    # so we need the intersection of both sets.
    relevant_tasks = testcomp_tasks.intersection(false_tasks)

    handle = partial(
        create_filtered_version,
        tasks_to_keep=relevant_tasks,
        output_directory=output_directory,
    )
    apply(handle, all_data)


main()


# In[ ]:


# After all data is prepared in directory 'raw-data', you can run tablegenerator -x table-defs/all.xml to get the HTML- and CSV-table for the results.
