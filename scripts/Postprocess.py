#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import tuftelike
from pathlib import Path

PROJECT_DIR = Path(__file__).parent.parent
TABLE_DEFS = PROJECT_DIR / "table-defs"
DATA = TABLE_DEFS / "all.table.csv"
SORT_BY_CORRECT = lambda kv: (-kv[1].get("correct confirmed", 0), -kv[1].get("confirmed by execution", 0))
assert DATA.exists()


# In[ ]:


from typing import Dict, Sequence, NamedTuple, Tuple
from collections import defaultdict
from functools import partial


class Measurement(NamedTuple):
    status: str
    category: str
    cputime: float


Row = Tuple[str, Dict[str, Measurement]]


def get_task_name(columns: list) -> str:
    return columns[0]


def get_tool_name(name: str) -> str:
    def strip_suffix(s, delim):
        if delim in s:
            return name[: s.find(delim)]
        return s

    name = strip_suffix(name, "SV-COMP23_unreach-call")
    name = strip_suffix(name, "Test-Comp23_coverage-error-call")
    name = strip_suffix(name, "ReachSafety-")
    name = strip_suffix(name, "SoftwareSystems-")
    if name.endswith("."):
        name = name[:-1]
    return name


def get_tool_for_column_index(column_index: int, header: list) -> str:
    name, _, sets = header[1][column_index].partition("[")
    sets = sets.lower()
    if "test-comp" in sets or "test-comp" in name.lower():
        assert not "sv-comp" in sets
        comp = "testcomp"
    elif "sv-comp" in sets or "sv-comp" in name.lower():
        assert not "test-comp" in sets
        comp = "svcomp"
    elif not sets:
        comp = "branches-testsuite"
    else:
        raise ValueError()
    name = get_tool_name(name)
    return f"{name}-{comp}"


def get_measurement_type_for_column_index(column_index: int, header: list) -> str:
    return header[2][column_index]


def get_row(row_as_text, header) -> Row:
    measurements_raw = defaultdict(dict)
    for i, column in enumerate(row_as_text):
        if i == 0:
            continue
        verifier_name = get_tool_for_column_index(i, header)
        measurement_type = get_measurement_type_for_column_index(i, header)
        measurements_raw[verifier_name][measurement_type] = column
    # filter all verifiers that have no value set
    measurements_raw = {
        k: v
        for k, v in measurements_raw.items()
        if v["status"] and v["category"] and _get_cputime(v)
    }
    measurements_parsed = {
        verifier_name: Measurement(
            status=measurements["status"],
            category=measurements["category"],
            cputime=float(_get_cputime(measurements)),
        )
        if measurements["status"]
        else None
        for verifier_name, measurements in measurements_raw.items()
    }
    return get_task_name(row_as_text), measurements_parsed


def _get_cputime(measurements):
    if 'cputime (s)' in measurements:
        return measurements['cputime (s)']
    elif 'cputime' in measurements:
        return measurements['cputime']
    else:
        return None


def parse_csv(file_name: Path, delimiter: str = "\t") -> Sequence[Row]:
    """Parse the given csv file and return a list of the rows.
    Each row is represented by a Tuple of the verification-task name
    and a dictionary of 'verifier-name' -> measurement for that verification task."""
    with open(file_name) as inp:
        lines = [line[:-1] if line[-1] == '\n' else line for line in inp]
        rows_as_text = [line.split(delimiter) for line in lines]

    # store the header separately:
    # first line of the CSV is 'tool  $TOOLNAME1 $TOOLNAME1 $TOOLNAME1 $TOOLNAME2 ...'
    # second line of the CSV is 'run set  $ID1 $ID1 $ID1 $ID2 ...'
    # third line of the CSV is 'sv-benchmarks/c/  status  category  cputime (s)  status  category ...'
    #
    # The header of one column (except for the first column) reads as follows:
    # "$TOOLNAME1 with benchmark config $ID1 produced measurement 'status'/'category'/..."
    # We don't care about the first line because the same tool may be used in different benchmark configs.
    # We use line 2 and line 3 of the header to extract the information
    # which verifier (config) produced which measurement.
    HEADER_END = 3
    header = rows_as_text[:HEADER_END]
    assert len(header) == 3

    transform = partial(get_row, header=header)
    return list(map(transform, rows_as_text[HEADER_END:]))


rows = parse_csv(DATA)
print(f"Verification tasks: {len(rows)}")
verifiers = sorted({v for row in rows for v in row[1]})
formal_verifiers = [v for v in verifiers if "svcomp" in v]
testers = [v for v in verifiers if "testcomp" in v]
testers_for_branch_coverage = [v for v in verifiers if "branches-testsuite" in v]
print(f"Verifiers used : {len(verifiers)}")
print(f"Formal verifiers ({len(formal_verifiers)}):\n{formal_verifiers}\n")
print(f"Testers ({len(testers)}):\n{testers}\n")
print(
    f"Testers for branch coverage ({len(testers_for_branch_coverage)}):\n{testers_for_branch_coverage}\n"
)
assert len(formal_verifiers) + len(testers) + len(testers_for_branch_coverage) == len(
    verifiers
)
# print(f"Example measurement: {rows[0][0]}\n{rows[0][1]}")


# In[ ]:


def filter_formal_verifiers(measurements: dict) -> dict:
    return {k: v for k, v in measurements.items() if k in formal_verifiers}


def filter_testers_cover_error(measurements: dict) -> dict:
    return {k: v for k, v in measurements.items() if k in testers}


def filter_testers_cover_branches(measurements: dict) -> dict:
    return {k: v for k, v in measurements.items() if k in testers_for_branch_coverage}


def filter_all_testers(measurements: dict) -> dict:
    return {
        k: v
        for k, v in measurements.items()
        if k in testers_for_branch_coverage or k in testers
    }


def create_union(measurements: dict) -> Sequence[Measurement]:
    def sort_by_cputime(measurements: Sequence[Measurement]) -> Sequence[Measurement]:
        return sorted(measurements, key=lambda m: m.cputime if m else 99999)

    if not measurements:
        return []

    correct_measurements = [
        m for m in measurements.values() if m and m.category == "correct"
    ]
    if correct_measurements:
        correct_by_cputime = sort_by_cputime(correct_measurements)
        return correct_by_cputime[0]
    return sort_by_cputime(measurements.values())[0]


def create_unions_for_row(row: Row) -> Row:
    task, measurements = row
    union_measurements = {
        "formal verifiers": create_union(filter_formal_verifiers(measurements)),
        "testers": create_union(filter_testers_cover_error(measurements)),
        "testers branch cov": create_union(
            filter_testers_cover_branches(measurements)
        ),
        "testers error + branch cov": create_union(
            filter_all_testers(measurements)
        ),
        "all": create_union(measurements),
    }
    return task, union_measurements


def create_union_for_rows(rows: Sequence[Row]) -> Sequence[Row]:
    """Take a sequence of rows of individual verifiers and create the unions for each of the rows.
    The unions are, per verification task: 'formal verifiers', 'testers', 'testers for branch coverage',
    'testers for error and branch coverage', and 'all'."""
    return list(map(create_unions_for_row, rows))


unions = create_union_for_rows(rows)


# In[ ]:


def get_statistics(rows: Sequence[Row]) -> Dict[str, Dict[str, float]]:
    """Return the overall statistics for a sequence of rows.
    Statistics are collected for each verifier for which measurements exist in at least one row.
    Statistics include a count for each category.

    Example structure of a returned dict:

    {
        'FuSeBMC': {
            'correct false': 758,
            'unknown': 20,
        }
        'CoVeriTest': {
            'correct false': 500,
            'unknown': 278,
        }
    }
    """
    stats = defaultdict(lambda: defaultdict(int))
    for _, measurements in rows:
        for verifier, measurement in measurements.items():
            if measurement:
                stats[verifier][measurement.category] += 1
    # remove defaultdict, return builtin dicts
    return {k: dict(v) for k, v in stats.items()}


def section(text):
    if isinstance(text, str):
        text = text.split("\n")
    dash_len = max(len(l) for l in text)
    print()
    print("-" * dash_len)
    print()
    print("\n".join(text))
    print()
    print("-" * dash_len)


def subsection(text):
    if isinstance(text, str):
        text = text.split("\n")
    dash_len = max(len(l) for l in text)
    print()
    print("\n".join(text))
    print("." * dash_len)


stats_each_tool = get_statistics(rows)
stats_unions = get_statistics(unions)

section("Overall Found Bugs")
print()
for tool, stats in stats_each_tool.items():
    print(f"{tool}: {stats.get('correct', 0)}")

section("Tools with 0 confirmed bugs")
print()
for tool, stats in stats_each_tool.items():
    if stats.get("correct", 0) == 0:
        print(tool)


# In[ ]:


def get_best(stats, n=3):
    """Return the best n verifiers of each type, for the given stats."""
    verifiers = filter_formal_verifiers(stats)
    testers_for_call = filter_testers_cover_error(stats)
    testers_for_branch = filter_testers_cover_branches(stats)

    all_the_best = []
    for group in [verifiers, testers_for_call, testers_for_branch]:
        group = list(
            sorted(group.items(), key=lambda kv: kv[1].get("correct", 0), reverse=True)
        )
        all_the_best += group[:n]
    return all_the_best


# In[ ]:


from functools import lru_cache


@lru_cache
def get_results_for_category(category: str, n=5):
    csv_file = TABLE_DEFS / f"{category}.table.csv"
    rows = parse_csv(csv_file)
    unions = create_union_for_rows(rows)
    stats_each_tool = get_statistics(rows)
    stats_unions = get_statistics(unions)
    return get_best(stats_each_tool, n=n), stats_unions


def get_all_categories():
    return [
        f"ReachSafety-{c}"
        for c in (
            "Arrays",
            "BitVectors",
            "ControlFlow",
            "ECA",
            "Floats",
            "Hardware",
            "Heap",
            "Loops",
            "ProductLines",
            "Recursive",
            "Sequentialized",
            "XCSP",
        )
    ] + [
        f"SoftwareSystems-{c}"
        for c in (
            "BusyBox",
            "DeviceDriversLinux64",
        )
    ]


def get_best_tools():
    best_tools = set()
    for category in get_all_categories():
        best_for_category, _ = get_results_for_category(category)
        # Take all tools that are within the best of the category.
        # To make sure that no tool with 0 correct results is the
        # 'best of the ones that can't solve anything', we only take
        # tools that solved at least one task correctly.
        # print(sorted(best_for_category, key=lambda kv: kv[1].get('correct', 0), reverse=True))
        best_tools |= {b[0] for b in best_for_category if b[1].get("correct", 0) > 0}
    return best_tools


# print()
# print("The best tools on the data set considered:")
# print(get_best_tools())


# In[ ]:


def get_results_per_category():
    for category in get_all_categories():
        best_for_category, unions = get_results_for_category(category)
        # Take all tools that are within the best of the category.
        # To make sure that no tool with 0 correct results is the
        # 'best of the ones that can't solve anything', we only take
        # tools that solved at least one task correctly.
        best_for_category = [b for b in best_for_category if b[1].get("correct", 0) > 0]
        yield category, best_for_category, unions


section(
    "The best tools per category, with confirmed bug reports\n(confirmed through witness validation or execution)"
)
print("\nCategory 'correct' is the number of correct and confirmed results (confirmed by any SV-COMP validator, not necessarily witness2test).")
print("Category 'correct-unconfirmed' is the number of correct results that were not confirmed. These tasks are a disjunct set of the correct and confirmed results and are not included in the number of 'correct'.")

for category, best_tools, union_results in get_results_per_category():
    best_tools = sorted(
        best_tools, key=lambda kv: kv[1].get("correct", 0), reverse=True
    )
    subsection(category)
    print("\n".join([f"{tool}: {results}" for tool, results in best_tools]))
    for k, v in union_results.items():
        print(f"{k}: {v}")


# In[ ]:


class GenerationResults(NamedTuple):
    tool_name: str
    error_call: int
    branches: int


#section(
#    "Comparison of bugs found with coverage-error-call\nand coverage-branches test suites"
#)
#def print_branch_coverage_hits_for_category(category):
#    suffix = "-with-wtt-validation-explicit.table.csv"
#    table_def = TABLE_DEFS / f"{category}{suffix}"
#    rows = parse_csv(table_def)
#    stats_each_tool = get_statistics(rows)
#    print(stats_each_tool)
#
#    subsection(f"Category {category} confirmed with branch-coverage suite")
#    for tool, stats in stats_each_tool.items():
#        if tool.endswith("-svcomp"):
#            continue
#        print(f"{tool}: {stats.get('correct', 0)}")
#
#error_tools = {k: v for k, v in stats_each_tool.items() if "testcomp" in k}
#results = list()
#for k, v in error_tools.items():
#    suffix = "-testcomp"
#    assert k.endswith(suffix)
#    tool_name = k[: -len(suffix)].lower()
#    corresponding_branch_generation = next(
#        (k, v)
#        for k, v in stats_each_tool.items()
#        if "testcov" in k and tool_name in k.lower()
#    )
#    results.append(
#        GenerationResults(
#            tool_name=tool_name,
#            error_call=v.get("correct", 0),
#            branches=corresponding_branch_generation[1].get("correct", 0),
#        )
#    )
#for result in sorted(
#    results, key=lambda t: max(t.error_call, t.branches), reverse=True
#):
#    text = result.tool_name
#    subsection(result.tool_name)
#    print(f"coverage-error-call: {result.error_call}")
#    print(f"coverage-branches: {result.branches}")


# In[ ]:


def merge_wtt(rows):
    svcomp_suffix = "-svcomp"
    testcomp_suffix = "-testcomp"
    merged_rows = []
    for task, measurements in rows:
        merged_measurements = dict()
        for_svcomp = [
            (k, v)
            for k, v in measurements.items()
            if k.endswith(svcomp_suffix) and "witness2test" not in k
        ]
        for tool, measurement in for_svcomp:
            if measurement.category != "correct":
                # if original result is not correct, there's no need to strengthen it with witness-to-test validation
                # because the result won't change.
                merged_measurements[tool] = measurement
                continue
            new_category = measurement.category
            cpawtt = measurements[
                f"cpa-witness2test-validate-violation-witnesses-{tool}"
            ]
            fshellwtt = measurements[
                f"fshell-witness2test-validate-violation-witnesses-{tool}"
            ]
            if cpawtt.category != "correct" and fshellwtt.category != "correct":
                new_category = "correct-unconfirmed"
            new_measurement = Measurement(
                category=new_category,
                cputime=measurement.cputime,
                status=measurement.status,
            )
            merged_measurements[tool] = new_measurement
        for tool, measurement in [
            (k, v) for k, v in measurements.items() if k.endswith(testcomp_suffix) or k.startswith('testcov-validate-test-suites')
        ]:
            merged_measurements[tool] = measurement
        merged_rows.append((task, merged_measurements))
    return merged_rows


def get_all_results_for_category(category):
    suffix = "-with-wtt-validation-explicit.table.csv"
    table_def = TABLE_DEFS / f"{category}{suffix}"
    rows = parse_csv(table_def)
    unions = create_union_for_rows(rows)
    stats_each_tool = get_statistics(rows)
    stats_unions = get_statistics(unions)
    wtt_rows = merge_wtt(rows)
    wtt_unions = create_union_for_rows(wtt_rows)
    wtt_stats_each_tool = get_statistics(wtt_rows)
    wtt_stats_unions = get_statistics(wtt_unions)
    return {
        'correct confirmed': stats_each_tool,
        'confirmed by execution': wtt_stats_each_tool,
        'unions confirmed': stats_unions,
        'unions confirmed by execution': wtt_stats_unions,
    }

def print_wtt_validation_results_for_category(category, results):
    testcomp_suffix = "-testcomp"
    svcomp_suffix = "-svcomp"
    stats_each_tool = results['correct confirmed']
    wtt_stats_each_tool = results['confirmed by execution']
    stats_unions = results['unions confirmed']
    wtt_stats_unions = results['unions confirmed by execution']

    subsection(f"Category {category} results")
    print(f"  {'tool name':50s} | {'correct':7s} | {'by execution':12s} | {'with branches':13s}")
    for tool, stats in sorted(stats_each_tool.items(), key=lambda kv: (kv[1].get("correct", 0), wtt_stats_each_tool.get(kv[0], dict()).get("correct", 0)), reverse=True):
        if not tool.endswith(testcomp_suffix) and not tool.endswith(svcomp_suffix):
            continue
        if tool.startswith("cpa-witness2test") or tool.startswith("fshell-witness2test"):
            continue
        tool_name = tool[: -len(testcomp_suffix)]
        try:
            corresponding_branch_generation = next(
                (k, v)
                for k, v in stats_each_tool.items()
                if "testcov" in k and tool_name.lower() in k.lower()
            )
        except StopIteration:
            corresponding_branch_generation = (None, dict())
        prefix="T" if tool.endswith(testcomp_suffix) else "S"
        print(f"{prefix:1s} {tool:50s} | {stats.get('correct', 0):7d} | {wtt_stats_each_tool.get(tool, dict()).get('correct', 0):12d} | {corresponding_branch_generation[1].get('correct', 0):13d}")
    print("-" * 60)
    for combi_name in ("testers", "testers branch cov", "testers error + branch cov", "formal verifiers", "all"):
        print(f"{combi_name:52s} | {stats_unions.get(combi_name, dict()).get('correct', 0):7d} | {wtt_stats_unions.get(combi_name, dict()).get('correct', 0):12d} | {0:13d}")


def print_overall_results_per_tool(all_results, excludes=[], sortkey=SORT_BY_CORRECT):
    subsection_str = "Over all categories"
    if excludes:
        subsection_str += f"\n(excluding: {excludes})"
    subsection(subsection_str)
    common_categories = [category for category in get_all_categories() if category not in excludes]
    all_tools = {tool for category in common_categories for tool in all_results[category]['correct confirmed'].keys()}
    tool_results = {tool: 
                    {
                        'correct confirmed': sum(all_results[category]['correct confirmed'].get(tool, dict()).get('correct', 0) for category in common_categories),
                        'confirmed by execution': sum(all_results[category]['confirmed by execution'].get(tool, dict()).get('correct', 0) for category in common_categories)
                    }
                    for tool in all_tools}
    print(f"  {'tool name':50s} | {'correct':7s} | {'by execution':12s} | {'with branches':13s}")
    for tool, results in sorted(tool_results.items(), key=sortkey):
        if not tool.endswith(testcomp_suffix) and not tool.endswith(svcomp_suffix):
            continue
        tool_name = tool[: -len(testcomp_suffix)]
        try:
            corresponding_branch_generation = next(
                (k, measurements['correct confirmed'])
                for k, measurements in tool_results.items()
                if "testcov" in k and tool_name.lower() in k.lower()
            )
        except StopIteration:
            corresponding_branch_generation = (None, 0)
        if tool.startswith("cpa-witness2test") or tool.startswith("fshell-witness2test"):
            continue
        prefix="T" if tool.endswith(testcomp_suffix) else "M"
        print(f"{prefix:1s} {tool:50s} | {results['correct confirmed']:7d} | {results['confirmed by execution']:12d} | {corresponding_branch_generation[1]:13d}")

section("Results for execution-based validation of reported bugs")
testcomp_suffix = "-testcomp"
svcomp_suffix = "-svcomp"
all_results = {category: get_all_results_for_category(category) for category in get_all_categories()}
for category, results in all_results.items():
    print_wtt_validation_results_for_category(category, results)
print_overall_results_per_tool(all_results, excludes=["ReachSafety-Hardware"])
print_overall_results_per_tool(all_results, excludes=[])
