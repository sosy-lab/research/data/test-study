#!/bin/bash

sudo swapoff -a
sudo chmod o+wt '/sys/fs/cgroup/cpuset'
sudo chmod o+wt '/sys/fs/cgroup/cpu,cpuacct'
sudo chmod o+wt '/sys/fs/cgroup/freezer'
sudo chmod o+wt '/sys/fs/cgroup/memory/user.slice/user-1000.slice/user@1000.service'
sudo chmod o+wt '/sys/fs/cgroup/pids/user.slice/user-1000.slice/user@1000.service'
sudo chmod o+wt '/sys/fs/cgroup/blkio'
