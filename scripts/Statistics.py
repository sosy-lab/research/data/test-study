# %%
from pathlib import Path
import sys
import bz2
from typing import Dict
import xml.etree.ElementTree as ET

import numpy

import tuftelike

import svbenchmark_statistics as stats

PROJECT_ROOT = Path(__file__).parent.parent
data_dir = PROJECT_ROOT / "raw-data"
representatives = list(
    data_dir.glob(
        "verifuzz.2022-12-21_23-42-42.results.Test-Comp23_coverage-error-call.*"
    )
)


def get_tasks(results_xml):
    with bz2.open(results_xml) as inp:
        content = ET.fromstring(inp.read())
    task_runs = content.findall("run")

    def get_file(run):
        taskdef = results_xml.parent / Path(run.get("name"))
        return stats.get_input_file(taskdef)

    correct_tasks = [
        t
        for t in task_runs
        if any(c.get("title") == "category" for c in t.findall("column"))
    ]
    return [get_file(run) for run in correct_tasks]

# %%
data_dir.exists()

# %%

tasks_per_category = dict()
for representative in representatives:
    category = representative.name.split(".")[4]
    tasks_per_category[category] = get_tasks(representative)
    for t in tasks_per_category[category]:
        assert t.exists(), f"{t} does not exist"

# %%
data: Dict[str, stats.Statistics] = {}
for category, tasks in tasks_per_category.items():
    data[category] = stats.collect_statistics(tasks)

data["Total"] = stats.Statistics(
    tasks={(t, c) for d in data.values() for t, c in d.tasks},
    LOC_sum=sum([d.LOC_sum for d in data.values()]),
    LOC_min=min([d.LOC_min for d in data.values()]),
    LOC_max=max([d.LOC_max for d in data.values()]),
    LOC_avg=numpy.mean([l for d in data.values() for _, l in d.tasks]),
    LOC_median=numpy.median([l for d in data.values() for _, l in d.tasks]),
    task_count=sum([d.task_count for d in data.values()]),
)



# %%
from matplotlib import pyplot as plt
from collections import Counter


def get_chart_value(v):
    return v


font = {"family": "normal", "weight": "normal", "size": 8}

plt.rc("font", **font)

output_folder = PROJECT_ROOT / "produced-data"
output_folder.mkdir(exist_ok=True)

def get_histogram(category_stats):
    locs = [l for _, l in category_stats.tasks]
    counts = Counter([get_chart_value(l) for l in locs])
    xs = [v for v, _ in counts.items()]
    ys = [c for _, c in counts.items()]
    return xs, ys

# add some padding to the values so that value 1 is better visible
padding = 5
max_y = max(
    y
    for name, category_stats in data.items()
    for y in get_histogram(category_stats)[1]
    if name != "Total"  # we don't use total, so ignore that
)
for category_name, category_stats in list(sorted(data.items(), key=lambda e: e[0])):
    print(
        f"Creating plot for {category_name}: n={len(category_stats.tasks)}",
        file=sys.stderr,
    )
    xs, ys = get_histogram(category_stats)
    ys_values = [y + padding for y in ys]
    distance = max(xs) - min(xs)
    # we choose a bar width so that the bars are about the size of a tick;
    # this makes it kind of visible when there are two bars close to each other.
    plt.bar(xs, ys_values, width=0.5 * distance / 40)
    # plt.xlabel(f"Lines of Code")
    # plt.ylabel("Amount", fontsize=7)
    # only adjust y-ticks to show scale from 1 to 21 if there is more than a single value
    tuftelike.adjust(xs, [min(ys_values), max_y+padding])
    # all plots should share the same y-axis scale
    plt.gca().set_ylim([0, max_y+padding])
    plt.gca().set_yticks([min(ys_values), max_y+padding], labels=[min(ys), max_y])
    plt.gca().set_xticks(
        [min(xs), int(min(xs) + (max(xs) - min(xs)) / 2), max(xs)],
        labels=[min(xs), int(min(xs) + (max(xs) - min(xs)) / 2), max(xs)],
    )
    plt.gcf().set_size_inches(2.5, 0.25)
    plt.savefig(
        str(output_folder / f"{category_name}.pdf"),
        bbox_inches="tight",
        transparent=True,
    )
    plt.clf()



# %%

# This produces the LaTeX commands for creating the overview table on the categories

for s, d in sorted(data.items(), key=lambda x: x[0]):
    s = s.replace("-", "").replace("64", "")
    for k in d._fields:
        if k == "tasks":
            continue
        name = k.replace("_", "")
        print("\\newcommand{\\" + s + name + "}{" + str(d.__getattribute__(k)) + "}")


