#!/bin/bash


PROJECT_DIR=$(realpath $(dirname "$0")/..)

cd "$PROJECT_DIR"/tools/testcov
RUNDEFS=$(grep "rundefinition name=" "$PROJECT_DIR"/benchmark-defs/testcov-validate-test-suites.xml \
    | grep -o '".*"' \
    | cut -c 2-\
    | rev \
    | cut -c 2- \
    | rev)
for rundef in $RUNDEFS; do
    "$PROJECT_DIR"/tools/benchexec/bin/benchexec \
        --read-only-dir / \
        --full-access-dir /sys/fs/cgroup \
        --overlay-dir /home \
        --overlay-dir . \
        --outputpath "../../raw-data/" \
        -r $rundef \
        "$PROJECT_DIR"/benchmark-defs/testcov-validate-test-suites.xml
done
