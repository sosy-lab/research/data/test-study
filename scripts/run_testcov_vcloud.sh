#!/bin/bash


PROJECT_DIR=$(realpath $(dirname "$0")/..)

cd "$PROJECT_DIR"/tools/testcov
RUNDEFS=$(grep "rundefinition name=" "$PROJECT_DIR"/benchmark-defs/testcov-validate-test-suites.xml \
    | grep -o '".*"' \
    | cut -c 2-\
    | rev \
    | cut -c 2- \
    | rev)
for rundef in $RUNDEFS; do
    "$PROJECT_DIR"/tools/benchexec/contrib/vcloud-benchmark.py \
        --vcloudClientHeap 6000 \
        --vcloudMaster vcloud-master.sosy-lab.org \
        --zipResultFiles \
        --cgroupAccess \
        --read-only-dir / \
        --overlay-dir /home/ \
        --overlay-dir ./ \
        --outputpath "$PROJECT_DIR/raw-data/" \
        -r $rundef \
        "$(realpath --relative-to . "$PROJECT_DIR")"/benchmark-defs/testcov-validate-test-suites.xml
done
