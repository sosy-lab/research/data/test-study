# Instructions Reproduction Package (VM Version)

This artifact requires at least 16 GB of RAM, 4 CPU cores, and 50 GB of disk space.

Open the terminal and switch into directory
`~/Test-Study`.
The README.md contains all further instructions.

Note: This VM already has all requirements installed.

VM Username: study
VM Password: study
