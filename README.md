# Reproduction Package for Article "Six Years Later: Testing vs. Model Checking"

Authors: Dirk Beyer and Thomas Lemberger
Year: 2023.

## Requirements

* make
* python >=3.8
* pip

To install the required python modules,
run 'pip install -r scripts/requirements.txt'.


## Replicating Analysis Data

After unzipping the artifact,
enter the unzipped directory and run `make` to reproduce our analysis results.
After initializing sv-benchmarks (~7 GB of download) and building results tables,
the statistics will be printed to the console,
and directory `produced-data/` will contain the plots that visualize the lines of code
per sv-benchmarks category.


## Replicating Everything

### Requirements

* gcc >= 8.0 with multilib for 32bit and 64bit support
* lcov >= 1.13
* clang-tidy (sometimes separate, sometimes included in clang package)
* [BenchExec][Benchexec-Releases] >= 1.10

[Benchexec-Releases]: https://github.com/sosy-lab/benchexec/releases

Python modules:

* lxml >= 4.0
* numpy >= 1.15
* pycparser >= 2.19

### Process

If you want to redo the postprocessing of the official SV-COMP and Test-Comp data,
as well as re-execute the experiments for test suites generated for coverage-branches,
run `make all`.

For this, you should have about 100GB of hard-drive space,
at least 10 GB of memory (RAM), and 4 cpu cores.

First, this will download three other artifacts:

* https://doi.org/10.5281/zenodo.7701122 "Results of the 5th Intl. Competition on Software Testing (Test-Comp 2023)"
* https://doi.org/10.5281/zenodo.7701126 "Test Suites from Test-Generation Tools (Test-Comp 2023)"
* https://doi.org/10.5281/zenodo.7627787 "Results of the 12th Intl. Competition on Software Verification (SV-COMP 2023)"

After the download, the TestCov experiments take about 30 hours of CPU~time on an Intel Xeon E3-1230 v5 @ 3.40 GHz.


### Troubleshooting

If there are errors due to wrong CGroup permissions, make sure that you use
cgroups v1, run `scripts/configure_cgroups.sh`
and make sure that your user is in group `benchexec`.

See the BenchExec documentation for more help:
https://github.com/sosy-lab/benchexec/blob/3.10/doc/INDEX.md
